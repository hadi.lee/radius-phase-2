'use strict';

import $ from 'jquery';
// import TweenLite from 'gsap'
// import Cookie from 'js-cookie';
import 'slick-carousel';

export default class Notification {
    constructor() {

        let $notification = $('.notification__content'),
            $notificationWrap = $('.notification'),
            $notificationBtn = $('.btn-toggle-notification'),
            $notificationCount = $('.icon-notification__count span', $notificationBtn),
            $btnClose = $('.notification .btn--close');

        var $status = $('.notification__paging');

        // Notification Cookies
        // if (Cookie.get('notice') == null || Cookie.get('notice') == 'true') {
        //   Cookie.set ('notice', 'true')
        //   TweenLite.from($('.notification'), .1, {
        //     y: "-10px",
        //     autoAlpha: 0
        //   });
        //   $('.notification').addClass('noticed');
		    // }

        $notification.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
            //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
            var i = (currentSlide ? currentSlide : 0) + 1;
            $status.text(i + '/' + slick.slideCount);
            $notificationCount.html(slick.slideCount);
        });

        var notifySlider = $notification.slick({
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            draggable: false,
            swipe: false,
            easing: 'swing',
            speed: 700,
            dots: false,
            arrows: true,
            prevArrow: $('.pagination__btn.prev'),
            nextArrow: $('.pagination__btn.next'),
            responsive: [ {
            breakpoint: 767,
              settings: {
                adaptiveHeight: true,
                swipe: true
              }
            },
            {
            breakpoint: 1024,
              settings: {
                adaptiveHeight: true,
                swipe: true
              }
            }
			    ]
        });

    	$btnClose.on('click', function(e) {
        e.preventDefault();

       $notificationWrap.slideUp(300);
            
       $notificationWrap.removeClass('noticed');
       $notificationWrap.removeClass('active');
            // Cookie.set('notice', 'false');
            $notification.slick('unslick');

    	});
  	}
}
