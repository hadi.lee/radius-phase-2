'use strict';

import $ from 'jquery';

export default class StickyShare {
	constructor() {
		
		let $shareContainer = $('.sticky-share'),
			$btnShare = $('.sticky-share__title', $shareContainer),
			$icon = $('li', $shareContainer);
		
		$btnShare.on('click', function(e) {
			console.log($icon);
			$icon.toggleClass('active');

		});

	}
}