'use strict';

import ScrollMagic from 'scrollmagic';
import {TweenMax, SteppedEase} from 'gsap';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js';

export default class TextCard {
	constructor() {
		//Brush stroke init controller
        
		var brushController1 = new ScrollMagic.Controller({
            globalSceneOptions: {
                triggerHook:0.5,
                // reverse: true
            }
		});

		var brushController2 = new ScrollMagic.Controller({
            globalSceneOptions: {
                triggerHook:1,
                // reverse: true
            }
		});

		var brushController3 = new ScrollMagic.Controller({
            globalSceneOptions: {
                triggerHook:1,
                // reverse: true
            }
		});

		var brushController4 = new ScrollMagic.Controller({
            globalSceneOptions: {
                triggerHook:1,
                // reverse: true
            }
		});


		//Build Brush 2 Scene
		var firstScene = new ScrollMagic.Scene({triggerElement: ".section-text-card__first", duration:2000})
			.setClassToggle('.brushstroke2-wrap', 'brushed')
			// .triggerHook("onCenter")
			// .addIndicators()
			.addTo(brushController2);

		var thirScene = new ScrollMagic.Scene({triggerElement: ".section-highlights", duration:2000})
			.setClassToggle('.brushstroke3-wrap', 'brushed')
			// .triggerHook("onCenter")
			// .addIndicators()
			.addTo(brushController3);

		var secondScene = new ScrollMagic.Scene({triggerElement: ".section-text-card__second", duration: 1200})
			.setClassToggle('.brushstroke1-wrap', 'brushed')
			// .triggerHook("onCenter")
			// .addIndicators()
			.addTo(brushController1);

		var secondScene = new ScrollMagic.Scene({triggerElement: ".section-text-card__second", duration: 1200})
			.setClassToggle('.brushstroke4-wrap', 'brushed')
			.addTo(brushController4);
		
		// Parallax init controller
		var parallaxController = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});

		
		new ScrollMagic.Scene({triggerElement: ".section-text-card__first"})
			.setTween(".section-text-card__first .text-card__title", {y: "50%", ease: Linear.easeNone})
			.addTo(parallaxController);
		
		new ScrollMagic.Scene({triggerElement: ".section-text-card__first"})
			.setTween(".section-text-card__first .text-card__content", {y: "50%", ease: Linear.easeNone})
			.addTo(parallaxController);

	}
}
