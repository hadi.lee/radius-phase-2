'use strict';

import $ from 'jquery';

export default class SiteNav {
  constructor() {

    let $siteNav = $('.site-nav');
    let $siteNavItems = $('li', $siteNav);
    let $lvl1 = $('.site-nav__list', $siteNav);
    let $lvl1Links = $('> li', $lvl1);

    this.$siteNav = $siteNav;
    this.$siteNavItems = $siteNavItems;


    $lvl1Links.map((j, lvl1Link) => {
      let $lvl1Link = $(lvl1Link);
      let $lvl1Item = $('> .site-nav__link', lvl1Link);
      let $lvl1Icon = $('<span class="icon icon-angle-down"></span>');

      if ($lvl1Link.find('.site-nav__lvl2').length) {
        $lvl1Link.find('> .site-nav__link').append($lvl1Icon);

        $lvl1Item.on('click', e => {

          // if href value is a hash, don't scroll to top
          if ($(e.target).attr('href') == "#") {
            e.preventDefault();
          }


          let expanded = $lvl1Link.hasClass('expanded');


          if (expanded) {
            this.collapseChild($lvl1Link);

          } else {
            this.expandChild($lvl1Link);
          }
        });
      }
    });

  }


  expandChild($parent) {
    $parent.data('expanded', true);
    $parent.addClass('expanded');

    let $expandedSibling = $parent.siblings('.expanded');
    this.collapseChild($expandedSibling);

    let $lvl2 = $('.site-nav__lvl2', $parent);
    $lvl2.slideDown();

    $parent.find('> .site-nav__link > .icon').removeClass('icon-angle-down').addClass('icon-angle-up');
  }

  collapseChild($parent) {
    $parent.data('expanded', false);
    $parent.removeClass('expanded');
    let $lvl2 = $('.site-nav__lvl2', $parent);
    $lvl2.slideUp();

    $parent.find('> .site-nav__link > .icon').removeClass('icon-angle-up').addClass('icon-angle-down');
  }

}
