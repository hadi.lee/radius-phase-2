'use strict';

import $ from 'jquery';
import 'slick-carousel';
import enquire from 'enquire.js';

export default class Carousel {
	constructor() {

		let baseOptions = {
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			rows: 0,
			speed: 600
		};

		if($('.section-highlights').length) {

			let $highlightsContent = $('.highlights__content'),
			$title = $('h2', $highlightsContent),
			$body = $('p', $highlightsContent),
			$a = $('a', $highlightsContent);


			let highlightsCarousel = {
				dots: true,
				arrows: true,
				swipeToSlide: true,
				autoplay: true,
				prevArrow: '<button type="button" class="slick-prev"><i class="icon icon-leftward"></i></button>',
				nextArrow: '<button type="button" class="slick-next"><i class="icon icon-rightward"></i></button>',
				appendArrows: '.highlights-carousel__nav-arrows',
				appendDots: '.highlights-carousel__nav-dots'
			}

			highlightsCarousel = $.extend({}, baseOptions, highlightsCarousel);

			$('.highlights [data-carousel]').slick(highlightsCarousel);


			$('.highlights [data-carousel]').on('beforeChange', function(event,slick,prevSlide,nextSlide) {
				
				let $nextEle = $(`.highlights__item[data-slick-index="${nextSlide}"]`);

				let title = $nextEle.data('title'),
				body = $nextEle.data('body'),
				url = $nextEle.data('url');


				$title.text(title);
				$body.text(body);
				$a.attr('href', url);


			})

		};

		if($('.standard-carousel').length){

			let standardCarousel = {
				dots: true,
				arrows: true,
				swipeToSlide: true,
				autoplay: true,
				prevArrow: '<button type="button" class="slick-prev"><i class="icon icon-leftward"></i></button>',
				nextArrow: '<button type="button" class="slick-next"><i class="icon icon-rightward"></i></button>',
				appendArrows: '.standard-carousel__nav-arrows',
				appendDots: '.standard-carousel__nav-dots'
			}

			standardCarousel = $.extend({}, baseOptions, standardCarousel);

			$('.standard-carousel [data-carousel]').slick(standardCarousel);
		};

	}
}
