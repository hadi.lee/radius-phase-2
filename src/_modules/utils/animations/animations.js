'use strict';

import $ from 'jquery';

export default class Animations {
	constructor() {
		let $animatedModule = $('[data-animate]');

		$animatedModule.map((i, ele) => {
			let offsetTop = $(ele).offset().top,
				scrollTrigger = $(window).height() * 1;
				

			let	$animateItem = $('[data-animation]', $(ele)),
				animationClass = $animateItem.data('animation');

			let offset = $(window).scrollTop() + $(window).height();	
			animate();

			$(window).on('scroll', function () {
				animate();
			});

			function animate() {
				if (($(window).scrollTop() + scrollTrigger) > offsetTop) {
					let interval = 150;
					// let ctaInterval = 150;

					$animateItem.map((j, ele) => {
						let $this = $(ele);

						setTimeout(function () {
							$this.addClass(animationClass);
						}, interval);

						interval += 100;
					});
				} else if (($(window).scrollTop() + scrollTrigger) < offset) {
					if ($animateItem.hasClass(animationClass)) {
						$animateItem.removeClass(animationClass);
					}
				}
			}

		});
	}
}
