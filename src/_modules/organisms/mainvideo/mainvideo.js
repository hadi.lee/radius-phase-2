"use strict";

import $ from "jquery";
import ScrollMagic from "scrollmagic";
import { TweenMax, SteppedEase } from "gsap";
import "scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js";
import "scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js";

import IScroll from "iscroll";

import _ from "underscore";

export default class MainVideo {
	constructor() {
		//Scrolling video
		if ($(".mainvideo").length) {
			let videoContainer = $("#video"),
				poster = videoContainer.data("poster"),
				mobileUrl = videoContainer.data('mobileUrl'),
				url = videoContainer.data("url");

			if (isMobile()) {

				url = mobileUrl;

			}

			var v = document.createElement("video");
			v.preload = "auto";
			v.muted = true;
			v.playsinline = true;
			v.setAttribute("playsinline", true);
			v.loop = "loop"; // enable loop

			document.getElementById("video").appendChild(v);

			v.poster = poster;
			v.src = url;

			

			v.play();

			// Scrolling video smooth
			// let winH = $(window).height(),
			// 	docObj = [
			// 		{
			// 			value: {
			// 				framerate: "60",
			// 				framelength: "144",
			// 				startscroll: "0.1",
			// 				endscroll: "1.0"
			// 			}
			// 		}
			// 	];

			// var myScroll = new IScroll("#scrollContainer");

			

			// let videoScroll = {
			// 	init: function(doc) {
			// 		var videos = _.map(doc, function(vid) {
			// 			var v = document.createElement("video");
			// 			v.preload = "auto";
			// 			v.muted = true;
			// 			v.playsinline = true;
			// 			v.setAttribute("playsinline", true);
			// 			// v.setAttribute("autoplay", true);

			// 			return _.extend(vid.value, {
			// 				vidEl: v,
			// 				scrollSetup: false,
			// 				setSrc: function(callback) {
			// 					var self = this;

			// 					this.timeStart = new Date();

			// 					document
			// 						.getElementById("video")
			// 						.appendChild(this.vidEl);

			// 					videoScroll.create(self);

			// 					this.vidEl.poster = poster;

			// 					this.vidEl.src = url;
			// 					this.vidEl.play();
			// 				}
			// 			});
			// 		});

			// 		var vidsLoaded = 0;
			// 		function LoadVideosAsync() {
			// 			if (vidsLoaded < videos.length)
			// 				videos[vidsLoaded].setSrc(LoadVideosAsync);
			// 			vidsLoaded++;
			// 		}
			// 		LoadVideosAsync();
			// 	},
			// 	create: function(obj) {
			// 		const scrollContainer = $(".mainvideo")[0],
			// 			video = obj.vidEl,
			// 			// label        = document.getElementById ('label_test'),
			// 			frameRate =
			// 				obj["framerate"] === undefined
			// 					? 30
			// 					: obj["framerate"],
			// 			frameLength =
			// 				obj["framelength"] === undefined
			// 					? 900
			// 					: obj["framelength"],
			// 			keyFrameLength =
			// 				obj["keyframelength"] === undefined
			// 					? 3
			// 					: obj["keyframelength"],
			// 			startScroll =
			// 				obj["startscroll"] === undefined
			// 					? 0
			// 					: obj["startscroll"],
			// 			endScroll =
			// 				obj["endscroll"] === undefined
			// 					? 1
			// 					: obj["endscroll"],
			// 			controller = new ScrollMagic.Controller({
			// 				container: window
			// 			}),
			// 			scene = new ScrollMagic.Scene({
			// 				triggerElement: scrollContainer,
			// 				triggerHook: "onCenter",
			// 				offset: -winH / 2
			// 			});

			// 		let scrollPercent = 0,
			// 			videoLastFrame = 0,
			// 			stepDivisor = 8,
			// 			updateVideo = false,
			// 			custTimer = null,
			// 			playThroughTimer = null,
			// 			readyState = false;

			// 		const startScrollAnimation = () => {
			// 			scene
			// 				.addTo(controller)
			// 				.duration(scrollContainer.clientHeight)
			// 				.on("progress", e => {
			// 					if (scrollPercent !== e.progress) {
			// 						scrollPercent = e.progress;

			// 						if (!updateVideo) {
			// 							adjustscrollPercent();
			// 						}
			// 					}
			// 				});
			// 		};

			// 		const jumpToTime = pct => {
			// 			let videoNextFrame = frameLength * pct,
			// 				videoCurrFrame =
			// 					videoLastFrame +
			// 					(videoNextFrame - videoLastFrame) / stepDivisor,
			// 				frameToUse = videoCurrFrame,
			// 				isBuffered = checkBuffered(frameToUse);

			// 			if (isBuffered) {
			// 				frameToUse = checkForPauses(frameToUse, pct);

			// 				video.currentTime = getTimeFromKeyframe(frameToUse);

			// 				if (
			// 					Math.abs(videoNextFrame - frameToUse) <
			// 					keyFrameLength + 1
			// 				)
			// 					updateVideo = false;
			// 				else updateVideo = true;
			// 			}

			// 			videoLastFrame = frameToUse;
			// 		};

			// 		const getTimeFromKeyframe = keyframe => {
			// 			return keyframe / frameRate;
			// 		};

			// 		const adjustscrollPercent = () => {
			// 			clearTimeout(custTimer);

			// 			if (
			// 				scrollPercent > startScroll &&
			// 				scrollPercent < endScroll
			// 			) {
			// 				let adjustedPct =
			// 					(scrollPercent - startScroll) /
			// 					(endScroll - startScroll);
			// 				adjustedPct =
			// 					adjustedPct > 1
			// 						? 1
			// 						: adjustedPct < 0
			// 						? 0
			// 						: adjustedPct;

			// 				custTimer = setTimeout(function() {
			// 					jumpToTime(adjustedPct);
			// 				}, 10);
			// 			} else if (scrollPercent <= startScroll)
			// 				custTimer = setTimeout(function() {
			// 					jumpToTime(0);
			// 				}, 10);
			// 			else if (scrollPercent >= endScroll)
			// 				custTimer = setTimeout(function() {
			// 					jumpToTime(1);
			// 				}, 10);
			// 		};

			// 		const checkBuffered = frameToUse => {
			// 			if (!readyState) {
			// 				return false;
			// 			} else {
			// 				return true;
			// 			}
			// 		};

			// 		const checkForPauses = (frameToUse, pct) => {
			// 			if (obj.pauses !== undefined) {
			// 				let pausesLen = obj.pauses.length;

			// 				while (pausesLen--) {
			// 					if (
			// 						pct > obj.pauses[pausesLen].start &&
			// 						pct < obj.pauses[pausesLen].end
			// 					) {
			// 						let adjNextFrame =
			// 								frameLength *
			// 								obj.pauses[pausesLen].start,
			// 							adjCurFrame =
			// 								videoLastFrame +
			// 								(adjNextFrame - videoLastFrame) /
			// 									stepDivisor;

			// 						return adjCurFrame;
			// 					}
			// 				}
			// 			}

			// 			return frameToUse;
			// 		};

			// 		video.addEventListener("timeupdate", function() {
			// 			if (updateVideo) {
			// 				adjustscrollPercent();
			// 				// clearTimeout (custTimer);
			// 				// custTimer = setTimeout (function () { jumpToTime (scrollPercent) }, 10);
			// 			} else {
			// 				clearTimeout(custTimer);
			// 			}
			// 		});

			// 		playThroughTimer = setInterval(function() {
			// 			if (video.readyState > 3) {
			// 				// console.log('video.readyState > 3')
			// 				video.pause();
			// 				clearInterval(playThroughTimer);

			// 				// video.currentTime = 0;
			// 				readyState = true;
			// 			}
			// 		}, 10);

			// 		startScrollAnimation();
			// 	}
			// };

			// videoScroll.init(docObj);

			function isMobile() {
				var isMobile = false; //initiate as false
				// device detection
				if (window.innerWidth <= 767) {
					isMobile = true;
				}

				return isMobile;
			}

			//Header init controller
			var headerController = new ScrollMagic.Controller({
				globalSceneOptions: {
					triggerHook: 0.3
				}
			});

			var shareController = new ScrollMagic.Controller({
				globalSceneOptions: {
					triggerHook: 0.1
				}
			});

			//Build Header Scene
			var scene = new ScrollMagic.Scene({
				triggerElement: ".section-text-card__first",
				duration: 0
			})
				.setClassToggle(".site-header", "is-active")
				.addTo(headerController);

			//Build Logo Scene
			var scene = new ScrollMagic.Scene({
				triggerElement: ".section-text-card__first",
				duration: 0
			})
				.setClassToggle(".sticky-share", "is-active")
				.addTo(shareController);

			//Video Content init controller
			var videocontentController = new ScrollMagic.Controller({
				globalSceneOptions: {
					triggerHook: 0.5,
					reverse: true
				}
			});

			$(".mainvideo__content--item").each(function() {
				//Video Content Tween
				var tweenTitle = TweenMax.to(
					$(this).find(".mainvideo__content--title"),
					0.2,
					{ y: 0, opacity: 1 }
				);
				var tweenPara = TweenMax.to(
					$(this).find(".mainvideo__content--para"),
					0.6,
					{ y: 0, opacity: 1 }
				);

				//Build Video Content Scene
				var scene = new ScrollMagic.Scene({ triggerElement: this })
					.setTween(tweenTitle)
					.addTo(videocontentController);

				var scene = new ScrollMagic.Scene({ triggerElement: this })
					.setTween(tweenPara)
					.addTo(videocontentController);
			});
		}

		// fix IE jumpy scroll issue
		if (navigator.userAgent.match(/Trident\/7\./)) {
			// if IE

			$(".mainvideo__container video").css("position", "static");

			$("body").on("mousewheel", function() {
				// remove default behavior
				event.preventDefault();

				//scroll without smoothing
				var wheelDelta = event.wheelDelta;
				var currentScrollPosition = window.pageYOffset;
				window.scrollTo(0, currentScrollPosition - wheelDelta);
			});
		}

		//scrolling video
	}
}
