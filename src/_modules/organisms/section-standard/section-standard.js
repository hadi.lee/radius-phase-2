'use strict';

import $ from 'jquery';

import ScrollMagic from 'scrollmagic';
import {TweenMax, SteppedEase} from 'gsap';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js';

export default class SectionStandard {
	constructor() {
		// // init controller
		// var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});

		// // build scenes
		// new ScrollMagic.Scene({triggerElement: ".parallax1"})
		// 				.setTween(".parallax1 > div", {y: "80%", ease: Linear.easeNone})
		// 				.addIndicators()
		// 				.addTo(controller);

		// new ScrollMagic.Scene({triggerElement: ".parallax2"})
		// 				.setTween(".parallax2 > div", {y: "80%", ease: Linear.easeNone})
		// 				.addIndicators()
		// 				.addTo(controller);

		// new ScrollMagic.Scene({triggerElement: ".parallax3"})
		// 				.setTween(".parallax3 > div", {y: "80%", ease: Linear.easeNone})
		// 				.addIndicators()
		// 				.addTo(controller);
		
		// new ScrollMagic.Scene({triggerElement: ".brushstroke-bg1"})
		// 				.setTween(".brushstroke-bg1 > div", {y: "80%", ease: Linear.easeNone})
		// 				.addIndicators()
		// 				.addTo(controller);

		// new ScrollMagic.Scene({triggerElement: ".brushstroke-bg2"})
		// 				.setTween(".brushstroke-bg2 > div", {y: "80%", ease: Linear.easeNone})
		// 				.addIndicators()
		// 				.addTo(controller);
		
		// new ScrollMagic.Scene({triggerElement: ".brushstroke-bg3"})
		// 				.setTween(".brushstroke-bg3 > div", {y: "80%", ease: Linear.easeNone})
		// 				.addIndicators()
		// 				.addTo(controller);
		

		

		//Brushstroke init controller
		var standardBrushController = new ScrollMagic.Controller({
			globalSceneOptions: {
				triggerHook:1,
				reverse: false
			}
		});

		$(".brushstroke-bg__item").each(function() {
			var brush = $(this).find('.brushstroke-wrap');
			//Build Brushstroke Scene
			var scene = new ScrollMagic.Scene({triggerElement: this, duration: 0})
				.setClassToggle(brush[0], 'brushed')
				.addTo(standardBrushController);
		});
	}
}
