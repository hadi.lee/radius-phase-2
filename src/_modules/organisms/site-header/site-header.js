'use strict';

import $ from 'jquery';
import SiteNav from '../../molecules/site-nav/site-nav';
import SiteSearch from '../../molecules/site-search/site-search';
import Notification from '../../molecules/notification/notification';

export default class SiteHeader {
  constructor() {
    let $siteHeader = $('.site-header');
    let $menuToggleBtn = $('.btn-toggle-menu', $siteHeader);
    let $mobileSearch = $('.m-site-search', $siteHeader);
    let $siteNav = $('.site-header__inner', $siteHeader);
    let $siteNavOverlay = $('.site-nav-overlay');

    this.menuOpened = false;
    this.searchOpened = false;

    this.$siteHeader = $siteHeader;
    this.$siteNav = $siteNav;
    this.$menuToggleBtn = $menuToggleBtn;
    this.$siteNavOverlay = $siteNavOverlay;

    this.$mobileSearch = $mobileSearch;

    this.siteNav = new SiteNav();
    this.siteSearch = new SiteSearch();
    this.notification = new Notification();

    $menuToggleBtn.on('click', e => {
      if (this.menuOpened) {
        this.closeMenu();
      } else {
        this.openMenu();
      }
    });


    $siteNavOverlay.on('click', e => {
      if (this.menuOpened) {
        this.closeMenu();
      } else {
        this.openMenu();
      }
    });

    $(document).on('click.headerCollapse', e => {
      let $eTarget = $(e.target);

      if (!($eTarget.hasClass('site-search') || $eTarget.parents('.site-search').length || $eTarget.hasClass('m-site-search') || $eTarget.parents('.m-site-search').length )) {
        this.closeSearch();
      }

      // if (!($eTarget.hasClass('site-nav') || $eTarget.parents('.site-nav').length || $eTarget.hasClass('site-nav__lvl2') || $eTarget.parents('.site-nav__lvl2').length || $eTarget.hasClass('btn-toggle-menu') || $eTarget.parents('.btn-toggle-menu').length)) {
      //   this.closeMenu();
      // }
    });

  }

  openMenu() {
    this.$siteHeader.addClass('menu-opened');
    this.$siteNav.addClass('is-opened');
    this.$menuToggleBtn.addClass('active');
    this.menuOpened = true;
    this.$mobileSearch.slideDown();
    this.$siteNavOverlay.show(0, () => {
      this.$siteNavOverlay.addClass('shown');
    });
  }

  closeMenu() {
    if (this.siteNav.lvl2Expanded) {
      this.siteNav.backLevelOne();
      setTimeout(() => {
        this.$siteHeader.removeClass('menu-opened');
        this.$siteNav.removeClass('is-opened');
        this.$menuToggleBtn.removeClass('active');
        this.menuOpened = false;
      }, 250);
    } else {
      this.$siteHeader.removeClass('menu-opened');
      this.$siteNav.removeClass('is-opened');
      this.$menuToggleBtn.removeClass('active');
      this.menuOpened = false;
    }
    this.$siteNavOverlay.removeClass('shown');
    setTimeout(() => {
      this.$siteNavOverlay.hide(0);
    }, 250);
  }

  openSearch() {
    this.siteSearch.open();
    this.$siteHeader.addClass('search-opened');
    this.searchOpened = true;
  }

  closeSearch() {
    this.siteSearch.close();
    this.$siteHeader.removeClass('search-opened');
    this.searchOpened = false;
  }
  
}
