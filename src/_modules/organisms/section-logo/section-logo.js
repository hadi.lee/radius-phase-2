"use strict";

import $ from "jquery";
import ScrollMagic from "scrollmagic";
import { TweenMax } from "gsap";
import "scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js";
import "scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js";

export default class SectionLogo {
	constructor() {

		setTimeout(function() {
			$('.section-logo__image--hide').addClass('reveal');
		}, 100);

		//Header init controller
		var logoController = new ScrollMagic.Controller({
			globalSceneOptions: {
				triggerHook: 0
			}
		});

		// //Header Tween
		var tween = TweenMax.to(".section-logo", 0.5, {
			backgroundColor: "rgba(255, 255, 255, 0)",
			opacity: "0"
		});

		var tweenLogo = TweenMax.to(".element-logo", 0.5, {
			scale: 5,
			opacity: 0,
			display: "none"
		});
		var tweenArrow = TweenMax.to(".section-logo__nav .icon", 0.5, {
			color: "#ffffff"
		});
		var tweenText = TweenMax.to(".section-logo__nav p", 0.5, {
			color: "#ffffff"
		});
		var tweenArrowHide = TweenMax.to(".section-logo__nav", 0.5, {
			display: "none"
		});

		//Build Header Scene
		var scene = new ScrollMagic.Scene({
			// triggerElement: "#section-logo",
			duration: 600
		})
			.setPin("#section-logo")
			.setTween(tween)
			.addTo(logoController);

		var scene = new ScrollMagic.Scene({
			triggerElement: "#section-logo",
			duration: 600
		})
			.setTween(tweenLogo)
			.addTo(logoController);

		var scene = new ScrollMagic.Scene({
			triggerElement: "#section-logo",
			duration: 600
		})
			.setTween(tweenArrow)
			.addTo(logoController);


		// hiding text above arrow
		var scene = new ScrollMagic.Scene({
			triggerElement: "#section-logo",
			duration: 600
		})
			.setTween(tweenText)
			.addTo(logoController);

		
		// hiding arrow
		var scene = new ScrollMagic.Scene({
			triggerElement: "#section-logo",
			duration: $(".mainvideo").height()
		})
			.setTween(tweenArrowHide)
			.addTo(logoController);



		//arrow skip video scroll to content
		let $arrow = $(".section-logo__nav .icon");


		$arrow.on("click", function() {
			
			$("html, body").animate(
				{
					scrollTop: $("#sticky").offset().top
				},
				1200
			);

			return false;
		});
	}
}
