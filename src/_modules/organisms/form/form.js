"use strict";

import $ from 'jquery';
import flatpickr from 'flatpickr';

export default class Form {
	constructor() {
		
		// remove jquery ui datepicker
		$('#ui-datepicker-div').remove();


		let dateInput = document.querySelectorAll('.form-control.datepicker');

		// hide value on load
		$(dateInput).val('');

		// apply flatpickr to element
		flatpickr(dateInput, {
			dateFormat: 'd-m-Y',
			maxDate: new Date()
		});

		

	}
}
