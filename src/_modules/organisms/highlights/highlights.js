'use strict';

import $ from 'jquery';
import ScrollMagic from 'scrollmagic';
import {TweenMax} from 'gsap';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js';

export default class Highlights {
	constructor() {
		// init controller
        var controller = new ScrollMagic.Controller({
            globalSceneOptions: {
                triggerHook: 0.3,
                reverse: true
            }
		});
		
		//Create Tween
		// var tween = TweenMax.to(".section-highlights__container", 0, {position:fixed});
		let windowH = $(window).height() + $('.section-collaborate').height() + $('.site-footer').height() + 100;
		
		//Build Scene
		var scene = new ScrollMagic.Scene({triggerElement: "#sticky", duration: windowH})
			.setClassToggle(".section-highlights__container", "active")
			// .addIndicators()
			.addTo(controller);
	}
}
