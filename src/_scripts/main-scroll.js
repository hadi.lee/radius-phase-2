'use script';

import $ from 'jquery';

import {TweenMax, Linear} from 'gsap';
import ScrollMagic from 'scrollmagic';
// import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
// import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';

export class homeScroll {
	constructor() {
        //Define brush images
        var images = [
            "../assets/images/brush01/01.png",
            "../assets/images/brush01/02.png",
            "../assets/images/brush01/03.png",
            "../assets/images/brush01/04.png",
            "../assets/images/brush01/05.png",
            "../assets/images/brush01/06.png",
            "../assets/images/brush01/07.png",
            "../assets/images/brush01/08.png",
            "../assets/images/brush01/09.png",
            "../assets/images/brush01/10.png",
            "../assets/images/brush01/11.png",
            "../assets/images/brush01/12.png",
            "../assets/images/brush01/13.png",
            "../assets/images/brush01/14.png",
            "../assets/images/brush01/15.png",
            "../assets/images/brush01/16.png",
            "../assets/images/brush01/17.png",
            "../assets/images/brush01/18.png",
        ];

        var obj = {curImg: 0};

        //Create brush tween
        var tween = TweenMax.to(obj, 0.5,
            {
                curImg: images.length - 1,	// animate propery curImg to number of images
                roundProps: "curImg",				// only integers so it can be used as an array index
                repeat: 3,									// repeat 3 times
                immediateRender: true,			// load first image automatically
                ease: Linear.easeNone,			// show every image the same ammount of time
                onUpdate: function () {
                  $("#brushImg").attr("src", images[obj.curImg]); // set the image source
                }
            }
        );

        //Brush stroke init controller
        var brushController = new ScrollMagic.Controller({
            globalSceneOptions: {
                triggerHook: 0,
                reverse: true
            }
        });

        //Build brushstroke scene
        new ScrollMagic.Scene({
            // triggerElement: '#sticky',
                duration: 300,
                reverse: true
            })
            .addIndicators()
            .setTween(tween)
            .addTo(brushController);
        
        
    }
}

$(() => {
    // let _homescroll = new homeScroll();
});