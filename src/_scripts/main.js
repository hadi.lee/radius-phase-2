// Main javascript entry point
// Should handle bootstrapping/starting application

"use strict";

import $ from "jquery";
import "jquery-match-height";
import objectFitImages from "object-fit-images";
import Rellax from "rellax";

import Carousel from "../_modules/utils/carousel/carousel";
import TableResponsive from "../_modules/utils/table-responsive/table-responsive";
import SelectClone from "../_modules/utils/select-clone/select-clone";
import BackToTop from "../_modules/utils/back-to-top/back-to-top";
import Animations from "../_modules/utils/animations/animations";

import StickyShare from "../_modules/molecules/sticky-share/sticky-share";
import TextCard from "../_modules/molecules/text-card/text-card";

import SiteHeader from "../_modules/organisms/site-header/site-header";
import MainVideo from "../_modules/organisms/mainvideo/mainvideo";
import Highlights from "../_modules/organisms/highlights/highlights";
import SectionLogo from "../_modules/organisms/section-logo/section-logo";
import SectionStandard from "../_modules/organisms/section-standard/section-standard";
import VideoTest from "../_modules/organisms/video-test/video-test";
import Form from "../_modules/organisms/form/form";

$(() => {
	//Polyfill for object-fit
	objectFitImages();

	// Apply wrapper for table
	if ($("table").length) {
		new TableResponsive();
	}

	new SiteHeader();
	new BackToTop();
	new SelectClone();

	if ($(".rellax").length) {
		var rellax = new Rellax(".rellax", {
			speed: -2,
			center: false,
			wrapper: null,
			round: true,
			vertical: true,
			horizontal: false
		});
	}

	if ($(".carousel").length) {
		new Carousel();
	}

	$(".match-height").matchHeight();

	if ($(".mainvideo").length) {
		new MainVideo();
	}

	if ($(".sticky-share").length) {
		new StickyShare();
	}

	if ($(".section-text-card").length) {
		new TextCard();
	}

	if ($(".highlights").length) {
		new Highlights();
	}

	if ($(".section-logo").length) {
		new SectionLogo();
	}

	new Animations();

	if ($(".section-standard").length) {
		new SectionStandard();
	}

	if ($(".video-test").length) {
		new VideoTest();
	}

	if ($(".sitecore-form").length) {
		new Form();
	}
});
